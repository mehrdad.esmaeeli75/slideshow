let slides = document.querySelectorAll(".slider");
let nextClick = document.querySelector(".next");
let prevClick = document.querySelector(".prev");
let dots = document.querySelectorAll(".dot");
let loading = document.getElementById("load");
let index = 0;
function slide() {
    if (index == slides.length - 1) {
        index = 0;
    } else {
        index++;
    }
    slides.forEach((z) => z.classList.remove("active"));
    slides[index].classList.add("active");
    dots.forEach((z) => z.classList.remove("active"));
    dots[index].classList.add("active");
}
nextClick.addEventListener("click", () => {
    slide();
});
prevClick.addEventListener("click", () => {
    if (index == slides.length) {
        index--;
    }
    if (index == 0) {
        index = slides.length - 1;
    } else {
        index--;
    }
    slides.forEach((z) => z.classList.remove("active"));
    slides[index].classList.add("active");
    dots.forEach((z) => z.classList.remove("active"));
    dots[index].classList.add("active");
});
setInterval(() => {
    loading.style.display = "none";
}, 4000);
setInterval(() => {
    slide();
}, 3000);
